import jinja2
import os
import glob
import click
import logging
import yaml
import itertools
import shutil
import coloredlogs

logger = logging.getLogger(__name__)


def render_template(template: str, data: dict) -> str:
    latex_jinja_env = jinja2.Environment(
        block_start_string=r'\BLOCK{',
        block_end_string='}',
        variable_start_string=r'\VAR{',
        variable_end_string='}',
        comment_start_string=r'\#{',
        comment_end_string='}',
        line_statement_prefix='%%',
        line_comment_prefix='%#',
        trim_blocks=True,
        autoescape=False,
        loader=jinja2.FileSystemLoader(os.path.abspath('/'))
    )
    template = latex_jinja_env.get_template(os.path.realpath(template))
    renderer_template = template.render(**data)
    return renderer_template


def to_file(document: str, path):
    path = os.path.abspath(path)

    dirname = os.path.dirname(path)
    if not os.path.exists(dirname):
        os.makedirs(dirname)

    with open(path, 'w') as f:
        f.write(document)


@click.command()
@click.argument('template_dir')
@click.argument('data_dir')
@click.argument('build_dir')
@click.option('--debug/--no-debug', default=False)
def make_templates(template_dir, data_dir, build_dir, debug):
    if debug:
        coloredlogs.install(level='DEBUG')
        logger.debug('Verbosity level set to DEBUG')
    else:
        coloredlogs.install(level='INFO')
        logger.info('Verbosity level set to INFO')

    data = [(os.path.splitext(os.path.basename(name))[0],
             yaml.load(open(name))) for name
            in glob.glob(os.path.join(data_dir, '*.yml'))]
    logger.info(f'Found {len(data)} data archives')

    templates = [(os.path.splitext(os.path.basename(temp))[0], temp)
                 for temp in glob.glob(os.path.join(template_dir, '*.tex'))]
    logger.info(f'Found {len(templates)} latex templates')

    if os.path.exists(build_dir):
        logger.debug('Build directory exists, deleting and recreating it')
        shutil.rmtree(build_dir)
        os.makedirs(build_dir)

    for (dname, data), (tname, template) in itertools.product(data, templates):
        filename = f'{tname}_{dname}.tex'
        logger.debug(f'Generating {filename}')
        render = render_template(template, data)
        to_file(render, os.path.join(build_dir, filename))
