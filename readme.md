# Curriculum Vitae

A continuous Integration Pipeline project to get nice curricula effortlessly.
## Where?
[Click here](https://gitlab.com/fralfaro/curriculum_vitae/-/jobs/artifacts/master/browse?job=generate_pdf) to browse the latest version of my curriculum

## Why?
I made this toy project as a means to:
  - Version my CV
  - Automatize the process to update my curriculum
  - Learn more about Continuous Integration.
  - Learn how to use [Gitlab CI/CD Pipelines](https://about.gitlab.com/product/continuous-integration/)  

## How?
The module `cv` has a single command `curriculum_vitae` that requires 3 
inputs to work: 
 - Template directory: A set of different curriculum templates
 - A data directory: Stores the actual data that will be put in the 
 curricula, all data must be in `*.yml` format.
 - Build/Output directory: The folder to output the latex render of each 
 curriculum made.
 
The command itself will generate all the combinations of data and templates,
 rendering a latex file for each one. After that, a call to `pdflatex 
 BUILD_DIR/*.tex` will compile all those files to `.pdf`
 
All this workflow is automated via [a pipeline](.gitlab-ci.yml) that 
triggers a build every time a commit is pushed, returning the `pdf` files as 
artifacts for the build.